Create Table Department1
(
	dnumber	INT,
	dname	VARCHAR(30),
	PRIMARY	KEY(dnumber)
)  ENGINE=InnoDB;
Create Table Employee1
(
	ssn	CHAR(9),
	name	VARCHAR(50),
	dno	INT,
	PRIMARY KEY(ssn),
	FOREIGN KEY(dno) REFERENCES Department1(dnumber)	
		ON DELETE SET NULL
		ON UPDATE CASCADE
)  ENGINE=InnoDB;
